<?php

use App\Models\User;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->id();
            $table->string('name')->unique();
            $table->float('height');
            $table->float('width');
            $table->float('weight');
            $table->boolean('is_received')->default(false);
            $table->timestamp('date_of_reception');
            $table->timestamp('date_of_received')->nullable();
            $table->timestamp('date_of_shipment');
            $table->unsignedBigInteger('client_id');
            $table->unsignedBigInteger('provider_id');
            $table->float('price');
            $table->string('photo')->nullable();
            $table->text('description');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
