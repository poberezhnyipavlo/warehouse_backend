<?php

namespace Database\Factories;

use App\Models\Product;
use Illuminate\Database\Eloquent\Factories\Factory;

class ProductFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Product::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->unique()->text(50),
            'height' => $this->faker->randomFloat(3, 0, 1000),
            'width' => $this->faker->randomFloat(3, 0, 1000),
            'weight' => $this->faker->randomFloat(3, 0, 100),
            'date_of_reception' => $this->faker->dateTime,
            'date_of_received' => $this->faker->dateTime,
            'date_of_shipment' => $this->faker->dateTime,
            'client_id' => 4,
            'provider_id' => 2,
            'price' => $this->faker->randomFloat(2, 0, 100),
            'description' =>  $this->faker->text,
        ];
    }
}
