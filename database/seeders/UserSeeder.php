<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $password = '123456';

        $userAdmin = User::create([
            'name' => 'admin',
            'email' => 'admin@admin.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userProvider1 = User::create([
            'name' => 'provider first',
            'email' => 'provider@first.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userProvider2 = User::create([
            'name' => 'provider second',
            'email' => 'provider@second.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userClient1 = User::create([
            'name' => 'client first',
            'email' => 'client@first.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userClient2 = User::create([
            'name' => 'client second',
            'email' => 'client@second.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userClient3 = User::create([
            'name' => 'client third',
            'email' => 'client@third.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userClient4 = User::create([
            'name' => 'client fourth',
            'email' => 'client@fourth.ua',
            'email_verified_at' => now(),
            'password' => Hash::make($password),
            'remember_token' => Str::random(10),
        ]);

        $userAdmin->assignRole(User::ADMIN);
        $userProvider1->assignRole(User::PROVIDER);
        $userProvider2->assignRole(User::PROVIDER);
        $userClient1->assignRole(User::CLIENT);
        $userClient2->assignRole(User::CLIENT);
        $userClient3->assignRole(User::CLIENT);
        $userClient4->assignRole(User::CLIENT);
    }
}
