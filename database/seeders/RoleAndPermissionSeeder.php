<?php

namespace Database\Seeders;

use App\Http\Controllers\Api\ProductController;
use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class RoleAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()[\Spatie\Permission\PermissionRegistrar::class]->forgetCachedPermissions();

        /**
         * @var Role $roleAdmin
         */
        $roleAdmin = Role::create(['name' => User::ADMIN]);
        /**
         * @var Role $roleProvider
         */
        $roleProvider = Role::create(['name' => User::PROVIDER]);
        /**
         * @var Role $roleClient
         */
        $roleClient = Role::create(['name' => User::CLIENT]);

        Permission::create([
            'name' => ProductController::PRODUCT_PERMISSION_DELETE,
        ]);
        Permission::create([
            'name' => ProductController::PRODUCT_PERMISSION_UPDATE,
        ]);
        Permission::create([
            'name' => ProductController::PRODUCT_PERMISSION_CREATE,
        ]);
        Permission::create([
            'name' => ProductController::PRODUCT_PERMISSION_VIEW,
        ]);

        $roleAdmin->givePermissionTo(
            ProductController::PRODUCT_PERMISSION_DELETE,
            ProductController::PRODUCT_PERMISSION_VIEW,
            ProductController::PRODUCT_PERMISSION_CREATE,
            ProductController::PRODUCT_PERMISSION_UPDATE,
        );

        $roleProvider->givePermissionTo(
            ProductController::PRODUCT_PERMISSION_DELETE,
            ProductController::PRODUCT_PERMISSION_VIEW,
            ProductController::PRODUCT_PERMISSION_CREATE,
            ProductController::PRODUCT_PERMISSION_UPDATE,
        );

        $roleClient->givePermissionTo(
            ProductController::PRODUCT_PERMISSION_VIEW,
        );
    }
}
