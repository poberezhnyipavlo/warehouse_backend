<?php

namespace App\Policies;

use App\Http\Controllers\Api\ProductController;
use App\Http\Services\Policy\UserPolicyService;
use App\Models\Product;
use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class ProductPolicy
{
    use HandlesAuthorization;

    protected UserPolicyService $service;

    public function __construct(UserPolicyService $service)
    {
        $this->service = $service;
    }

    /**
     * Determine whether the user can view any models.
     *
     * @param User $user
     * @return mixed
     */
    public function viewAny(User $user): bool
    {
        return $user->hasPermissionTo(
            ProductController::PRODUCT_PERMISSION_VIEW
        );
    }

    /**
     * Determine whether the user can view the model.
     *
     * @param User $user
     * @param Product $product
     * @return mixed
     */
    public function view(User $user, Product $product): bool
    {
        if ($user->hasPermissionTo(
            ProductController::PRODUCT_PERMISSION_VIEW
        )) {
            return $this->service->checkAccess($user, $product);
        }

        return false;
    }

    /**
     * Determine whether the user can create models.
     *
     * @param User $user
     * @return mixed
     */
    public function create(User $user): bool
    {
        return $user->hasPermissionTo(
            ProductController::PRODUCT_PERMISSION_CREATE
        );
    }

    /**
     * Determine whether the user can update the model.
     *
     * @param User $user
     * @param Product $product
     * @return mixed
     */
    public function update(User $user, Product $product): bool
    {
        if ($user->hasPermissionTo(
            ProductController::PRODUCT_PERMISSION_UPDATE
        )) {
            return $this->service->checkAccess($user, $product);
        }

        return false;
    }

    /**
     * Determine whether the user can delete the model.
     *
     * @param User $user
     * @param Product $product
     * @return mixed
     */
    public function delete(User $user, Product $product): bool
    {
        if ($user->hasPermissionTo(
            ProductController::PRODUCT_PERMISSION_DELETE
        )) {
            return $this->service->checkAccess($user, $product);
        }

        return false;
    }
}
