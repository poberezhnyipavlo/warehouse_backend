<?php

namespace App\Http\Resources;

use Illuminate\Http\Request;
use Illuminate\Http\Resources\Json\JsonResource;

class ProductResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name' => $this->name,
            'height' => $this->height,
            'width' => $this->width,
            'weight' => $this->weight,
            'is_received' => $this->is_received,
            'date_of_reception' => $this->date_of_reception,
            'date_of_received' => $this->date_of_received ?? null,
            'date_of_shipment' => $this->date_of_shipment,
            'client' => new ClientResource($this->client),
            'price' => $this->price,
            'photo' => $this->photo ?? null,
            'description' => $this->description,
        ];
    }
}
