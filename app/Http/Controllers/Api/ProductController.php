<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\ProductStoreRequest;
use App\Http\Requests\ProductUpdateRequest;
use App\Http\Resources\ProductResource;
use App\Http\Services\ProductService;
use App\Models\Product;
use App\Models\User;
use Illuminate\Http\JsonResponse;

class ProductController extends Controller
{
    public const PRODUCT_PERMISSION_CREATE = 'product create';
    public const PRODUCT_PERMISSION_VIEW = 'product view';
    public const PRODUCT_PERMISSION_UPDATE = 'product update';
    public const PRODUCT_PERMISSION_DELETE = 'product delete';

    protected ProductService $service;

    public function __construct(ProductService $productService)
    {
        $this->service = $productService;

        $this->authorizeResource(Product::class, 'product');
    }

    /**
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        return response()->json(
            ProductResource::collection(
                $this->service->getAll($user)
            )
        );
    }

    /**
     * @param ProductStoreRequest $request
     * @return JsonResponse
     */
    public function store(ProductStoreRequest $request): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        return response()->json(
            new ProductResource(
                $this->service->save($request->validated(), $user)
            )
        );
    }

    /**
     * @param Product $product
     * @return JsonResponse
     */
    public function show(Product $product): JsonResponse
    {
        return response()->json(new ProductResource($product));
    }

    /**
     * @param ProductUpdateRequest $request
     * @param Product $product
     * @return JsonResponse
     */
    public function update(ProductUpdateRequest $request, Product $product): JsonResponse
    {
        return response()->json(
            new ProductResource(
                $this->service->update($request->validated(), $product)
            )
        );
    }

    /**
     * @param Product $product
     * @return JsonResponse
     * @throws \Exception
     */
    public function destroy(Product $product): JsonResponse
    {
        /**
         * @var User $user
         */
        $user = auth()->user();

        if ($this->service->delete($product, $user)) {
            return response()->json();
        }
    }
}
