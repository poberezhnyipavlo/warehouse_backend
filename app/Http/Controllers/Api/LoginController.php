<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\LoginUserRequest;
use App\Models\User;
use Illuminate\Http\JsonResponse;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /**
     * @param LoginUserRequest $request
     * @return JsonResponse
     */
    public function __invoke(LoginUserRequest $request): JsonResponse
    {
        Auth::attempt($request->validated());

        /**
         * @var User $user
         */
        $user = \auth()->user();

        if ($user) {
            $token = $user->createToken('bearer');
            return \response()->json(['token' => $token->plainTextToken], 200);
        }

        return \response()->json(['error' => 'Ви ввели не правильний емейл або пароль'], 401);
    }
}
