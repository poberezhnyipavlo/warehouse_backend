<?php

namespace App\Http\Services\Policy;

use App\Models\Product;
use App\Models\User;

class UserPolicyService
{
    /**
     * @param User $user
     * @param Product $product
     * @return bool
     */
    public function checkAccess(User $user, Product $product): bool
    {
        if ($user->hasRole(User::ADMIN)) {
            return true;
        }

        if ($user->hasRole(User::PROVIDER) && $product->provider_id === $user->id) {
            return true;
        }

        if ($user->hasRole(User::CLIENT) && $product->client_id === $user->id) {
            return true;
        }

        return false;
    }
}
