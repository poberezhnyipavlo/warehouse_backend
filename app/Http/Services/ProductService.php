<?php

namespace App\Http\Services;

use App\Models\Product;
use App\Models\User;
use Illuminate\Database\Eloquent\Collection;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class ProductService
{
    /**
     * @param User $user
     * @return Collection|null
     */
    public function getAll(User $user): ?Collection
    {
        if ($user->hasRole(User::ADMIN)) {
            return Product::all();
        }

        if ($user->hasRole(User::PROVIDER)) {
            return $user->providerProducts()->get();
        }

        if ($user->hasRole(User::CLIENT)) {
            return $user->clientProducts()->get();
        }

        return null;
    }

    /**
     * @param array $data
     * @param User $user
     * @return Product
     */
    public function save(array $data, User $user): Product
    {
        $product = new Product($data);
        $product->provider_id = $user->id;
        $product->save();

        if (isset($data['photo'])) {
            $product->photo = $this->uploadFile($data['photo'], $product);

            $product->save();
        }

        return $product;
    }

    /**
     * @param array $data
     * @param Product $product
     * @return Product
     */
    public function update(array $data, Product $product): Product
    {
        $product->update($data);

        if (isset($data['photo'])) {
            $product->photo = $this->uploadFile($data['photo'], $product);

            $product->save();
        }

        return $product;
    }

    /**
     * @param Product $product
     * @param User $user
     * @return bool
     * @throws \Exception
     */
    public function delete(Product $product, User $user): bool
    {
        if ($user->hasRole([User::PROVIDER, User::ADMIN])) {
            if ($product->delete()) {
                return true;
            }
        }

        abort(405, 'Ви не можете видалити цей товар.');
    }

    /**
     * @param UploadedFile $file
     * @param Product $product
     * @return string
     */
    private function uploadFile(UploadedFile $file, Product $product): string
    {
        Storage::putFileAs(Product::URL, $file, $product->id . '.' . $file->extension());

        return Product::URL . $product->id . '.' . $file->extension();
    }
}
