<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;

class ProductStoreRequest extends BaseProductRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return array_merge_recursive(
            parent::rules(),
            [
                'name' => [
                    'required',
                    'string',
                    Rule::unique('products', 'name'),
                    'min:1',
                ],
            ]
        );
    }
}
