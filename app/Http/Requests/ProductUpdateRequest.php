<?php

namespace App\Http\Requests;

class ProductUpdateRequest extends BaseProductRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return array_merge_recursive(
            parent::rules(),
            [
                'name' => [
                    'required',
                    'string',
                    'min:1',
                ],
            ]);
    }
}
