<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

abstract class BaseProductRequest extends FormRequest
{
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules(): array
    {
        return [
            'height' => [
                'required',
                'numeric',
            ],
            'width' => [
                'required',
                'numeric',
            ],
            'weight' => [
                'required',
                'numeric',
            ],
            'date_of_reception' => [
                'required',
                'date',
            ],
            'date_of_shipment' => [
                'required',
                'date',
            ],
            'client_id' => [
                'required',
                'integer',
                Rule::exists('users', 'id'),
            ],
            'price' =>[
                'required',
                'numeric',
            ],
            'photo' => [
                'nullable',
                'file',
                'image',
                'mimes:jpeg,png',
            ],
            'description' => [
                'string',
                'required',
            ],
        ];
    }
}
