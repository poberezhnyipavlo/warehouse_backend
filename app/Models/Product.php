<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Support\Carbon;

/**
 * App\Models\Product
 *
 * @property int $id
 * @property string $name
 * @property float $height
 * @property float $width
 * @property float $weight
 * @property Carbon $date_of_reception
 * @property Carbon $date_of_shipment
 * @property int $client_id
 * @property int $provider_id
 * @property float $price
 * @property string $photo
 * @property string $description
 * @property Carbon|null $created_at
 * @property Carbon|null $updated_at
 * @property-read User $client
 * @property-read User $provider
 * @method static Builder|Product newModelQuery()
 * @method static Builder|Product newQuery()
 * @method static Builder|Product query()
 * @method static Builder|Product whereClientId($value)
 * @method static Builder|Product whereCreatedAt($value)
 * @method static Builder|Product whereDateOfReception($value)
 * @method static Builder|Product whereDateOfShipment($value)
 * @method static Builder|Product whereDescription($value)
 * @method static Builder|Product whereHeight($value)
 * @method static Builder|Product whereId($value)
 * @method static Builder|Product whereName($value)
 * @method static Builder|Product wherePhoto($value)
 * @method static Builder|Product wherePrice($value)
 * @method static Builder|Product whereProviderId($value)
 * @method static Builder|Product whereUpdatedAt($value)
 * @method static Builder|Product whereWeight($value)
 * @method static Builder|Product whereWidth($value)
 * @mixin \Eloquent
 */
class Product extends Model
{
    use HasFactory;

    public const URL = '/products/';

    protected $fillable = [
        'name',
        'height',
        'width',
        'weight',
        'is_received',
        'date_of_reception',
        'date_of_received',
        'date_of_shipment',
        'client_id',
        'parent_id',
        'price',
        'photo',
        'description',
    ];

    public function client(): BelongsTo
    {
        return $this->belongsTo(User::class, 'client_id');
    }

    public function provider(): BelongsTo
    {
        return $this->belongsTo(User::class, 'provider_id');
    }
}
