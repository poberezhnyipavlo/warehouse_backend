<?php

namespace Tests\Feature\Client;

use Tests\TestCase;
use Tests\Traits\ProductTrait;

class ProductTest extends TestCase
{
    use ProductTrait;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loginClient();

        $this->fakeProducts(self::COUNT_PRODUCTS);
    }

    public function testGetAllWithAuth(): void
    {
        $this
            ->getJson(self::PRODUCT_URL)
            ->assertOk()
            ->assertJsonStructure([self::structureApi()]);
    }
}
