<?php

namespace Tests\Feature\Admin;

use Illuminate\Http\Response;
use Tests\TestCase;
use Tests\Traits\ProductTrait;

class ProductTest extends TestCase
{
    use ProductTrait;

    protected function setUp(): void
    {
        parent::setUp();

        $this->loginAdmin();

        $this->fakeProducts(self::COUNT_PRODUCTS);
    }

    public function testGetAllWithAuth(): void
    {
        $this
            ->getJson(self::PRODUCT_URL)
            ->assertOk()
            ->assertJsonStructure([self::structureApi()]);
    }

    /**
     * @test
     */
    public function testCreate(): void
    {
        $this
            ->postJson(self::PRODUCT_URL, $this->payloadProduct())
            ->assertOk();
    }

    public function testUpdate(): void
    {
        $this
            ->putJson(self::PRODUCT_URL . '2', $this->payloadProduct())
            ->assertOk();
    }

    public function testDelete(): void
    {
        $this
            ->deleteJson(self::PRODUCT_URL . '2')
            ->assertOk();
    }

    /**
     * @dataProvider getInvalidData
     * @param array $input
     * @param array $output
     */
    public function testFailedValidate(array $input, array $output): void
    {
        $this
            ->postJson(self::PRODUCT_URL, $input)
            ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
            ->assertJsonFragment(
                $this->generateErrorMessages(...$output)
            );
    }
}
