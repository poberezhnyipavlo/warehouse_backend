<?php

namespace Tests\Feature\User;

use Illuminate\Http\Response;
use Tests\TestCase;
use Tests\TraitErrorMessages\LoginErrorMessageTrait;

class LoginTest extends TestCase
{
    use LoginErrorMessageTrait;

    public function testLogin(): void
    {
        $data = [
            'email' => 'admin@admin.ua',
            'password' => '123456',
        ];

        $this
            ->postJson(self::LOGIN_URL, $data)
            ->assertOk()
            ->assertJsonStructure(self::structureApi());
   }

   public function testFailedLogin(): void
   {
       $data = [
           'email' => 'admin@admin.ua',
           'password' => '123456 222',
       ];

       $this
           ->postJson(self::LOGIN_URL, $data)
           ->assertStatus(Response::HTTP_UNAUTHORIZED);
   }

    /**
     * @dataProvider getInvalidData
     * @param array $input
     * @param array $output
     */
    public function testValidateLogin(array $input, array $output): void
   {
       $this
           ->postJson(self::LOGIN_URL, $input)
           ->assertStatus(Response::HTTP_UNPROCESSABLE_ENTITY)
           ->assertJsonFragment(
               $this->generateErrorMessages(...$output)
           );
   }

    private function structureApi(): array
    {
        return [
            'token',
        ];
    }

    public function getInvalidData(): array
    {
        return [
            'empty password' => [
                'input' => [
                    'email' => 'admin@admin.ua',
                ],
                'output' => [
                    self::errorPasswordRequired()
                ],
            ],
            'empty email' => [
                'input' => [
                    'password' => '123456',
                ],
                'output' => [
                    self::errorEmailRequired()
                ],
            ],
            'email format' => [
                'input' => [
                    'email' => 'adminadmin.ua',
                    'password' => '123456',
                ],
                'output' => [
                    self::errorEmailFormat()
                ],
            ],
            'not in db email' => [
                'input' => [
                    'email' => 'admin232323@admin.ua',
                    'password' => '123456',
                ],
                'output' => [
                    self::errorEmailNotInDB()
                ],
            ]
        ];
    }
}
