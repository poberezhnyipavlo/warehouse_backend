<?php

namespace Tests;

use Illuminate\Foundation\Testing\TestCase as BaseTestCase;

abstract class TestCase extends BaseTestCase
{
    use CreatesApplication;

    private const PASSWORD = '123456';

    public const LOGIN_URL = 'api/login/';
    public const PRODUCT_URL = 'api/product/';

    public const COUNT_PRODUCTS = 5;

    protected function setUp(): void
    {
        parent::setUp();

        $this->artisan('migrate:fresh');
        $this->artisan('db:seed');
    }

    protected function loginAdmin(): void
    {
        $email = 'admin@admin.ua';

        self::login($email);
    }

    protected function loginProvider(): void
    {
        $email = 'provider@first.ua';

        self::login($email);
    }

    protected function loginClient(): void
    {
        $email = 'client@first.ua';

        self::login($email);
    }

    protected function login(string $email): void
    {
        $response = $this->postJson(self::LOGIN_URL, [
            'email' => $email,
            'password' => self::PASSWORD,
        ]);

        $this->withHeader('Authorization', 'Bearer ' . $response->json('token'));
    }

    protected const UNPROCESSABLE_ENTITY_MESSAGE = ['message' => 'The given data was invalid.'];

    /**
     * @param mixed ...$errors
     * @return array
     */
    protected function generateErrorMessages(...$errors): array
    {
        return ['errors' => array_merge_recursive(...$errors)] + self::UNPROCESSABLE_ENTITY_MESSAGE;
    }
}
