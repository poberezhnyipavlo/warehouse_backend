<?php

namespace Tests\Traits;

use App\Models\Product;
use Faker\Factory as Faker;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Tests\TraitErrorMessages\ProductErrorMessageTrait;

trait ProductTrait
{
    use HasFactory, ProductErrorMessageTrait;

    protected function fakeProducts(int $count = 1): void
    {
        Product::factory()->count($count)->create();
    }

    protected function makeProduct(): Product
    {
        return Product::factory()->make();
    }

    protected function payloadProduct(array $product = []): array
    {
        /**
         * @var Faker $faker
         */
        $faker = Faker::create();

        return [
            'name' => $product['name'] ?? $faker->unique()->text(50),
            'height' => $product['height'] ?? $faker->randomFloat(3, 0, 1000),
            'width' => $product['width'] ?? $faker->randomFloat(3, 0, 1000),
            'weight' => $product['weight'] ?? $faker->randomFloat(3, 0, 100),
            'date_of_reception' => $product['date_of_reception'] ?? $faker->date,
            'date_of_received' => $product['date_of_received'] ?? $faker->date,
            'date_of_shipment' => $product['date_of_shipment'] ?? $faker->date,
            'client_id' => 4,
            'provider_id' => 2,
            'price' => $product['price'] ?? $faker->randomFloat(2, 0, 100),
            'description' => $product['description'] ?? $faker->text,
        ];
    }

    protected function structureApi(): array
    {
        return [
            'name',
            'height',
            'width',
            'weight',
            'is_received',
            'date_of_reception',
            'date_of_received',
            'date_of_shipment',
            'client' => [
                'name',
                'email',
            ],
            'price',
            'photo',
            'description',
        ];
    }

    public function getInvalidData(): array
    {
        /**
         * @var Faker $faker
         */
        $faker = Faker::create();


        return [
            'required name' => [
                'input' => [
                    'height' => $faker->randomFloat(3, 0, 1000),
                    'width' => $faker->randomFloat(3, 0, 1000),
                    'weight' => $faker->randomFloat(3, 0, 100),
                    'date_of_reception' => $faker->date,
                    'date_of_received' => $faker->date,
                    'date_of_shipment' => $faker->date,
                    'client_id' => 4,
                    'provider_id' => 2,
                    'price' => $faker->randomFloat(2, 0, 100),
                    'description' => $faker->text,
                ],
                'output' => [
                    self::errorNameRequired(),
                ],
            ],
            'string height width weight' => [
                'input' => [
                    'name' => $faker->unique()->text(50),
                    'height' => $faker->text(),
                    'width' => $faker->text(),
                    'weight' => $faker->text(),
                    'date_of_reception' => $faker->date,
                    'date_of_received' => $faker->date,
                    'date_of_shipment' => $faker->date,
                    'client_id' => 4,
                    'provider_id' => 2,
                    'price' => $faker->randomFloat(2, 0, 100),
                    'description' => $faker->text,
                ],
                'output' => [
                    self::errorHeightNotNumber(),
                    self::errorWidthNotNumber(),
                    self::errorWeightNotNumber(),
                ],
            ],
            'not in bd client' => [
                'input' => [
                    'name' => $faker->unique()->text(50),
                    'height' => $faker->randomFloat(3, 0, 1000),
                    'width' => $faker->randomFloat(3, 0, 1000),
                    'weight' => $faker->randomFloat(3, 0, 100),
                    'date_of_reception' => $faker->date,
                    'date_of_received' => $faker->date,
                    'date_of_shipment' => $faker->date,
                    'client_id' => 99999,
                    'price' => $faker->randomFloat(2, 0, 100),
                    'description' => $faker->text,
                ],
                'output' => [
                    self::errorClientNotInBD(),
                ],
            ],
        ];
    }
}
