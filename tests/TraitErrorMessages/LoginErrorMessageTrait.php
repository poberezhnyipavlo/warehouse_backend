<?php

namespace Tests\TraitErrorMessages;

trait LoginErrorMessageTrait
{
    protected function errorEmailRequired()
    {
        return ['email' => ['The email field is required.']];
    }

    protected function errorPasswordRequired()
    {
        return ['password' => ['The password field is required.']];
    }

    protected function errorEmailFormat()
    {
        return ['email' => ['The email must be a valid email address.']];
    }

    protected function errorEmailNotInDB()
    {
        return ['email' => ['The selected email is invalid.']];
    }
}
