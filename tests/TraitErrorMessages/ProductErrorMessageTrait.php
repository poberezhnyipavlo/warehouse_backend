<?php

namespace Tests\TraitErrorMessages;

trait ProductErrorMessageTrait
{
    protected function errorNameRequired()
    {
        return ['name' => ['The name field is required.']];
    }

    protected function errorHeightNotNumber()
    {
        return ['height' => ['The height must be a number.']];
    }

    protected function errorWidthNotNumber()
    {
        return ['width' => ['The width must be a number.']];
    }

    protected function errorWeightNotNumber()
    {
        return ['weight' => ['The weight must be a number.']];
    }

    protected function errorClientNotInBD()
    {
        return ['client_id' => ['The selected client id is invalid.']];
    }
}
